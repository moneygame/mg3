(function() {
  'use strict';

  angular
    .module('moneygame.auth')
    .controller('AuthController', AuthController);

  /* @ngInject */
  function AuthController($state, $q, $ionicLoading, $ionicPopup, authService) {

    var vm = this;
    vm.continueWithoutFB = continueWithoutFB;
    vm.loginFB = loginFB;

    function continueWithoutFB() {
      authService.loginGuest();
      $state.go('home');
    }

    function loginFB() {
      $ionicLoading.show({
        template: 'Logging in...'
      });

      authService.loginFB(loginFBSuccess, loginFBFailure);
    }

    function loginFBSuccess(response) {
      $ionicLoading.hide();
      $state.go('home');
    }

    function loginFBFailure(response) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Error',
        template: 'Login Failed. Try again.'
      });
    }


  }

})();

