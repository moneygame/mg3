(function() {
  'use strict';

  angular
    .module('moneygame.auth')
    .service('authService', authService);

  /* @ngInject */
  function authService($q, userService) {

    this.loginFB = loginFB;
    this.loginGuest = loginGuest;
    this.verifyLoggedIn = verifyLoggedIn;

    function loginFB(successCallback, failureCallback) {
      facebookConnectPlugin.login(['email', 'public_profile'],
          function(response) {
            loginFBSuccess(response, successCallback, failureCallback);
          },
          function(response) {
            loginFBFailure(response, failureCallback);
          });
    }

    function loginGuest() {
      userService.setGuestUser();
    }

    function verifyLoggedIn(successCallback, failureCallback) {

      facebookConnectPlugin.getLoginStatus(
          function(success) {
            loginFBSuccess(success, successCallback, failureCallback);
          },
          function(failure) {
            failureCallback();
          });
    }

    function loginFBSuccess(fbResponse, successCallback, failureCallback) {

      var authResponse;

      if (!fbResponse.authResponse) {

        failureCallback("Cannot find the authResponse");

      } else {

        authResponse = fbResponse.authResponse;

        getUserFBInfo(authResponse)
          .then(function() {
            successCallback(fbResponse);
          });

      }

    }

    function loginFBFailure(response, failureCallback) {
      failureCallback(response);
    }

    function getUserFBInfo(authResponse) {
      var info = $q.defer();

      facebookConnectPlugin.api(
          '/me?fields=email,first_name,picture.width(400).height(400),age_range,gender,location,birthday,devices&access_token=' + authResponse.accessToken,
          null,
          function(userInfo) {
            info.resolve(userInfo);
          },
          function(error) {
            info.reject(error);
          }
      );

      return info.promise.then(function(userInfo) {
          userService.setFBUser(userInfo)
        });
    }

  }

})();

