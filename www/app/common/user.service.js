(function() {
  'use strict';

  angular
    .module('moneygame')
    .service('userService', userService);

  /* @ngInject */
  function userService($http, $q, HOST) {

    this.getUser = getUser;
    this.logout = logout;
    this.setFBUser = setFBUser;
    this.setGuestUser = setGuestUser;
    this.updateUserGameProgress = updateUserGameProgress;
    this.getModuleLevel = getModuleLevel;
    this.levelUpPlayerModuleLevel = levelUpPlayerModuleLevel;

    var localUser = {};
    var deferredUserData = $q.defer();
    var userDataPromise = deferredUserData.promise;

    activate();

    function activate() {
      initUser();
    }

    function getUser() {
      return userDataPromise.then(function() {
        return localUser;
      });
    }

    function logout() {
      initUser();
    }

    function setFBUser(userInfo) {

      var fbUserData = {
        player: {
          facebook_id: userInfo.id,
          email: userInfo.email,
          gender: userInfo.gender,
          birthdate: userInfo.birthday,
          address: userInfo.location,
          name: userInfo.first_name,
          avatar_url: userInfo.picture.data.url,
          device_name: getFirstDeviceName(userInfo.devices)
        }
      }

      return registerUser(fbUserData);
    }

    function getFirstDeviceName(devices) {
      return (devices == undefined) ? null : devices[0].os;
    }

    function setGuestUser() {
      return registerUser(localUser);
    }

    function getModuleLevel(moduleId) {
      var result;
      angular.forEach(localUser.player.module_levels, function (moduleLevel) {
        if(moduleId === moduleLevel.game_module_id) {
          result = moduleLevel;
          return;
        }
      });

      return result;
    }

    function levelUpPlayerModuleLevel(currentLevel) {
      angular.forEach(localUser.player.module_levels, function (level, key) {
        if(level.game_module_id === currentLevel.module_level.game_module_id){
          localUser.player.module_levels[key] = currentLevel.module_level;
          return;
        }
      });
    }

    function updateUserGameProgress(data) {
      localUser.player.points += data.pointsIncrease;
      localUser.player.level += data.levelIncrease;
      registerProgressUpdate();
    }

    function initUser() {
      setUser({
        player: {
          id: '',
          facebook_id: '',
          email: '',
          gender: '',
          birthdate: '',
          address: '',
          name: '',
          avatar_url: '',
          level: 1,
          module_levels: [],
          points: 0,
          badges_count: 0,
          device_name: ''
        },
        player_id: window.localStorage.getItem('mg_user') || ''
      });
    }

    function registerProgressUpdate() {
      return $http.put(HOST + '/players/' + localUser.player.id + '/level_up', localUser);
    }

    function registerUser(localUserData) {
      return $http({
          method: 'PUT',
          url: HOST + 'players/login',
          data: localUserData
        })
        .success(function(completeUserData) {
          setUser(completeUserData);
          deferredUserData.resolve();
          if (!window.localStorage.getItem('mg_user')) {
            window.localStorage.setItem('mg_user', completeUserData.player.id);
          }
        });
    }

    function setUser(user) {
      localUser = user;
    }

  }

})();

