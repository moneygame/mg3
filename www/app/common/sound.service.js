(function() {
  'use strict';

  angular
    .module('moneygame')
    .service('soundService', soundService);

  /* @ngInject */
  function soundService($window) {

    this.playNegativeSound = playNegativeSound;
    this.playPositiveSound = playPositiveSound;
    this.preloadSounds = preloadSounds;

    var $audio = $window.plugins.NativeAudio;
    var numPositiveSounds;
    var numNegativeSounds;

    function playNegativeSound() {
      var random = Math.floor(Math.random() * numNegativeSounds) + 1;
      $audio.play('neg' + random);
    }

    function playPositiveSound() {
      var random = Math.floor(Math.random() * numPositiveSounds) + 1;
      $audio.play('pos' + random);
    }

    function preloadSounds() {
      $audio.preloadSimple('pos1', 'audio/gtz-pos01.mp3');
      $audio.preloadSimple('pos2', 'audio/gtz-pos02.mp3');
      $audio.preloadSimple('pos3', 'audio/gtz-pos03.mp3');
      $audio.preloadSimple('pos4', 'audio/gtz-pos04.mp3');
      $audio.preloadSimple('pos5', 'audio/gtz-pos05.mp3');
      numPositiveSounds = 5;

      $audio.preloadSimple('neg1', 'audio/gtz-neg01.mp3');
      $audio.preloadSimple('neg2', 'audio/gtz-neg02.mp3');
      $audio.preloadSimple('neg3', 'audio/gtz-neg03.mp3');
      $audio.preloadSimple('neg4', 'audio/gtz-neg04.mp3');
      $audio.preloadSimple('neg5', 'audio/gtz-neg05.mp3');
      numNegativeSounds = 5;
    }

  }

})();

