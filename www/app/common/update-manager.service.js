(function(){
  'use strict';

  angular
    .module('moneygame')
    .service('updateManagerService', updateManagerService);

  function updateManagerService($ionicDeploy,
                                $ionicLoading) {
    
    this.downloadUpdates = downloadUpdates;

    function downloadUpdates() {
      $ionicDeploy.channel = 'dev';
      
      $ionicDeploy.check()
        .then(function(snapshotAvailable) {
          if(snapshotAvailable) {
            $ionicLoading.show({ 
              template: 'Updating...'
            })
            .then(function() {
              $ionicDeploy.download()
                .then(extractUpdate)
                .then($ionicLoading.hide)
                .then(loadUpdate);
            });
          }
        });
    }

    function extractUpdate() { 
      return $ionicDeploy.extract(); 
    }

    function loadUpdate() {
      $ionicDeploy.load();
    }

  }

})();
