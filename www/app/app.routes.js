(function() {
  'use strict';

  angular
    .module('moneygame')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/auth/auth.html',
        controller: 'AuthController as vm'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'app/home/home.html',
        controller: 'ModulesMenuController as vm'
      })
      .state('profile', {
        url: '/profile',
        cache: false,
        templateUrl: 'app/profile/profile.html',
        controller: 'ProfileController as vm'
      })
      .state('tools', {
        url: '/tools',
        cache: false,
        templateUrl: 'app/tools/tools.html',
        controller: 'ToolsController as vm'
      })
      .state('game', {
        cache: false,
        url: '/game',
        templateUrl: 'app/game/game.html',
        controller: 'GameController as vm',
        params: {
          module: {value: 1},
          moduleName: {value: ''},
          hiddenParam: 'YES'
        }
      })
      .state('levelComplete', {
        url: '/level-complete',
        templateUrl: 'app/game/level-complete.html'
      })
      .state('loanCalculator', {
        url: '/loan-calculator',
        templateUrl: 'app/tools/loan-calculator/loan-calculator.html',
        controller: 'LoanCalculatorController as vm'
      })
      .state('savingsCalculator', {
        url: '/savings-calculator',
        templateUrl: 'app/tools/savings-calculator/savings-calculator.html',
        controller: 'SavingsCalculatorController as vm'
      })
      .state('coaching', {
        url: '/coaching',
        templateUrl: 'app/tools/coaching/coaching.html',
        controller: 'CoachingController as vm'
      })
      .state('dictionary', {
        url: '/dictionary',
        templateUrl: 'app/tools/dictionary/dictionary.html',
        controller: 'DictionaryController as vm'
      });

  }

})();

