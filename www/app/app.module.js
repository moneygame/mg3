(function() {
  'use strict';

  angular
    .module('moneygame', [
        'ionic',
        'ui.router',
        'ionic.cloud',
        'moneygame.auth',
        'moneygame.profile',
        'moneygame.analytics',
        'moneygame.tools',
        'moneygame.game',
        'moneygame.dictionary',
        'moneygame.coaching'
    ])
    .config(function($ionicCloudProvider) {
      $ionicCloudProvider.init({
        'core': {
          'app_id': 'e2a5da02'
        }
      });
    })
    .run(function($ionicPlatform, $ionicHistory, $ionicPopup, $state, $rootScope,
                  authService, GOOGLE_ANALYTICS_ID, $ionicDeploy) {

      $ionicDeploy.channel = 'dev';
      $ionicDeploy.check().then(function(snapshotAvailable) {
        if(snapshotAvailable) {
          $ionicDeploy.download()
            .then(function() {
              return $ionicDeploy.extract()
            })
          .then(function () {
            $ionicDeploy.load();
          });
        }
      });

      $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        if (error === 'nope') {
          event.preventDefault();
          $state.go('home');
        }
      });

      $ionicPlatform.ready(function() {
        if(typeof analytics !== 'undefined') {
          analytics.startTrackerWithId(GOOGLE_ANALYTICS_ID);
        }

        $ionicPlatform.registerBackButtonAction(function(event) {

          // Handle Android back button to avoid the application exits accidentaly
          if ($state.current.name == 'home') {
            $ionicPopup.confirm({
              title: 'System warning',
              template: 'Are you sure you want to exit?'
            }).then(function(res) {
              if (res) {
                ionic.Platform.exitApp();
              }
            });
          } else {
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
              historyRoot: true
            });
            $state.go('home');
          }
        }, 100);

        // Hide the accessory bar by default (remove this to show the
        // accessory bar above the keyboard for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar) {
          StatusBar.styleDefault();
        }

        authService.verifyLoggedIn(verifyLoginSuccess, verifyLoginFailure);

      });

      function verifyLoginSuccess() {
        $state.go('home');
      }

      function verifyLoginFailure() {
        $state.go('login');
      }

    });

})();

