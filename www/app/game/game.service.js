(function() {
  'use strict';

  angular
    .module('moneygame.game')
    .service('gameService', gameService);

  /* @ngInject */
  function gameService($http, userService, HOST) {

    this.cleanUp = cleanUp;
    this.getGameData = getGameData;
    this.getUserData = getUserData;
    this.setProgress = setProgress;
    this.updateUserGameProgress = updateUserGameProgress;
    this.levelUpModule = levelUpModule;
    this.setCurrentModule = setCurrentModule;
    this.getCurrentModule = getCurrentModule;

    var progress, isInProgress, currentModule;

    function setCurrentModule(module) {
      currentModule = module;
    }

    function getCurrentModule() {
      return currentModule;
    }

    function cleanUp() {
      isInProgress = false;
      progress = null;
    }

    function getGameData() {

      if (isInProgress) {
        return Promise.resolve(progress);
      } else {
        return getQuestions()
          .then(function(_questionData_) {
            return {
              questionData: _questionData_,
              questionCounter: 0,
              points: 0
            };
          });
      }
    }

    function getUserData() {
      return userService.getUser()
        .then(function(userObj) {
          return userObj.player;
        });
    }

    function setProgress(_progress_) {
      isInProgress = true;
      progress = _progress_;
    }

    function updateUserGameProgress(data) {
      userService.updateUserGameProgress(data);
    }

    function getQuestions() {
      var moduleId = currentModule.module;
      var moduleLevel = userService.getModuleLevel(moduleId);
      return $http.get(HOST + 'game_modules/' + moduleLevel.game_module_id + '/' +  moduleLevel.level + '/questions')
        .then(function(response) {
          return response.data.questions;
        });
    }

    function levelUpModule(playerId) {
      var moduleId = currentModule.module;
      return $http.put(HOST + 'module_levels/' + playerId + '/' + moduleId + '/level_up')
    }

  }

})();

