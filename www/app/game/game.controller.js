(function() {
  'use strict';

  angular
    .module('moneygame.game')
    .controller('GameController', GameController);

  /* @ngInject */
  function GameController($stateParams, $q, gameService, userService, $state, $timeout, $log, $ionicLoading, $scope, $ionicModal, soundService, analyticsService) {

    var vm = this;
    vm.numAnswered = 0;
    vm.questionCounter = 0;
    vm.points = 0;

    vm.goBack = goBack;
    vm.goToNextQuestion = goToNextQuestion;
    vm.goToPrevQuestion = goToPrevQuestion;
    vm.goToMainMenu = goToMainMenu;
    vm.checkAnswer = checkAnswer;
    vm.showHint = showHint;
    vm.toArray = toArray;

    $scope.exit = exit;

    var questionData, modal;
    var POINTS_PER_CORRECT = 10;
    var correctToast = document.getElementById('correct-toast');
    var incorrectToast = document.getElementById('incorrect-toast');
    var countWrongAnswers = 0;
    var ionicLoadingPromise;

    activate();

    function activate() {
      ionicLoadingPromise = $ionicLoading.show();
      analyticsService.setViewAnalytics($state.current.name);
      soundService.preloadSounds();
      loadGameData()
        .then(loadGameDataSuccess)
        .catch(function() {
          showModal('app/game/module-completed.html');
        })
        .finally(hideIonicLoading);
    }

    function hideIonicLoading() {
      return ionicLoadingPromise.then($ionicLoading.hide);
    }

    function loadGameDataSuccess() {
      goToNextAction();
    }

    function goBack($event) {
      showExitWarning($event);
    }

    function goToNextQuestion() {
      if (vm.questionCounter < vm.numAnswered) {
        vm.questionCounter += 1;
        setNextQuestion();
      }
    }

    function goToPrevQuestion() {
      if (vm.questionCounter > 0) {
        vm.questionCounter -= 1;
        setNextQuestion();
      }
    }

    function checkAnswer(item) {

      item.answered = true;
      if (item.correct) {

        soundService.playPositiveSound();
        showToast(correctToast, 500);

        analyticsService.setCountTries("Wrong answer", gameService.getCurrentModule().moduleName, questionData[vm.questionCounter].body, countWrongAnswers);
        countWrongAnswers = 0;
        vm.questionCounter += 1;

        if (vm.questionCounter > vm.numAnswered) {
          vm.numAnswered += 1;
          vm.points += POINTS_PER_CORRECT;
        }

        saveProgress();
        $timeout(function() {
          goToNextAction();
        }, 500);

      } else {

        countWrongAnswers += 1;
        showToast(incorrectToast, 1000);
        soundService.playNegativeSound();
        saveProgress();

      }
    }

    function showHint() {
      $scope.hint = questionData[vm.questionCounter].hint;
      showModal('app/game/hint.html');
    }

    function goToNextAction() {
      if (vm.questionCounter < vm.TOTAL_QUESTIONS) {
        setNextQuestion();
      } else {
        gameService.getUserData()
          .then(function(user) {
            gameService.levelUpModule(user.id)
              .then(handleLevelUpSuccess)
              .catch(handleLevelUpError);
          });
      }
    }

    function handleLevelUpSuccess(response) {

      gameService.cleanUp();
      gameService.updateUserGameProgress({
        levelIncrease: 1,
        pointsIncrease: vm.points
      });
      userService.levelUpPlayerModuleLevel(response.data);
      $state.go('levelComplete');
    }

    function handleLevelUpError(response) {
      $log.error(response.data);
    }

    function loadGameData() {
      var currentModule = gameService.getCurrentModule();
      var deferred = $q.defer();

      gameService.getGameData()
        .then(function(gameData) {
          if(gameData.questionData) {
            vm.questionCounter = gameData.questionCounter;
            vm.numAnswered = vm.questionCounter;
            questionData = gameData.questionData;
            vm.TOTAL_QUESTIONS =  questionData.length;
            vm.currentModuleLevel = userService.getModuleLevel(currentModule.module);
            vm.points = gameData.points;
            deferred.resolve();
          } else {
            deferred.reject();
          }
        });
      return deferred.promise;
    }

    function saveProgress() {
      gameService.setProgress({
        questionCounter: vm.questionCounter,
        questionData: questionData,
        points: vm.points
      });
    }

    function setNextQuestion() {
      vm.question = questionData[vm.questionCounter].body;
      vm.answerOptions = questionData[vm.questionCounter].options;
      vm.hint = questionData[vm.questionCounter].hint;
    }

    function showToast(toast, duration) {
      hideToasts();

      toast.style.opacity= 1;
      $timeout(function() {
        toast.style.opacity = 0;
      }, duration);
    }

    function hideToasts() {
      correctToast.style.opacity = 0;
      incorrectToast.style.opacity = 0;
    }

    function toArray(number) {
      var array = [];
      for (var i=0; i<number; i++) {
        array.push(i);
      }
      return array;
    }

    function exit() {
      gameService.cleanUp();
      modal.remove();
      $state.go('home');
    }

    function showExitWarning() {
      showModal('app/game/exit-alert.html');
    }

    function goToMainMenu() {
      $state.go('home');
    }

    function showModal(templateUrl) {
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope
      })
      .then(function(_modal_) {
        modal = _modal_;
        $scope.modal = modal;
        modal.show();
      });
    }
  }

})();

