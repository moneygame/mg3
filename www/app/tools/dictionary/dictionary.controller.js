(function() {
  'use strict';

  angular
    .module('moneygame.dictionary')
    .controller('DictionaryController', DictionaryController);

  /* @ngInject */
  function DictionaryController($ionicModal, $scope, $q, $state, dictionaryService, analyticsService) {

    var vm = this;
    vm.showDefinition = showDefinition;
    vm.groupedTerms;
    vm.definitionModal;
    vm.dictionaryTerm;
    var initialIndexes = [];

    activate();

    function activate() {
      analyticsService.setViewAnalytics($state.current.name);
      dictionaryService.getDictionaryTerms()
        .then(groupTermsByInitial);
    }

    function showDefinition(dictionaryTerm) {
      vm.dictionaryTerm = dictionaryTerm;
      analyticsService.setViewAnalytics("definition");
      showModal('app/tools/dictionary/definition.html')
        .then(function(modal) {
          vm.definitionModal = modal;
        });
    }

    function groupTermsByInitial(dictionaryTerms) {
      vm.groupedTerms = [];
      var initial, index;

      angular.forEach(dictionaryTerms, function(dictionaryTerm) {
        initial = dictionaryTerm.term[0];
        index = initialIndexes.indexOf(initial);

        if (index === -1) {
          initialIndexes.push(initial);
          vm.groupedTerms.push({
            initial: initial,
            terms: [dictionaryTerm]
          });
        } else {
          vm.groupedTerms[index].terms.push(dictionaryTerm);
        }

      });
    }

    function showModal(templateUrl) {
      var modal = $q.defer();

      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope
      })
      .then(function(_modal_) {
        modal.resolve(_modal_);
        _modal_.show();
      });

      return modal.promise;
    }

  }

})();

