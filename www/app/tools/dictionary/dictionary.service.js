(function() {
  'use strict';

  angular
    .module('moneygame.dictionary')
    .service('dictionaryService', dictionaryService);

  /* @ngInject */
  function dictionaryService($http, HOST) {
    this.getDictionaryTerms = getDictionaryTerms;

    function getDictionaryTerms() {
      return $http.get(HOST + 'dictionary_terms')
        .then(function(dictionaryTerms) {
          return dictionaryTerms.data.dictionary_terms;
        });
    }
  }

})();

