(function() {
  'use strict';

  angular
    .module('moneygame.coaching')
    .controller('CoachingController', CoachingController);

  /* @ngInject */
  function CoachingController($sce, $state, analyticsService) {
    var vm = this;
    var COACHING_PAGE = 'http://www.unitedwayatlanta.org/the-challenge/income/financial-capability-network/'
    vm.loadCoachingPageUrl = loadCoachingPageUrl;

    function loadCoachingPageUrl() {
      analyticsService.setViewAnalytics($state.current.name);
      return $sce.trustAsResourceUrl(COACHING_PAGE);
    }
  }

})();

