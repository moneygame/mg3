(function() {
  'use strict';

  angular
    .module('moneygame.tools')
    .controller('ToolsController', ToolsController);

  /* @ngInject */
  function ToolsController($ionicModal, $scope, $q, $state, loanCalculatorService, savingsCalculatorService, $timeout, analyticsService) {

    var vm = this;
    vm.showLoanCalculator = showLoanCalculator;
    vm.showLoanCalcResult = showLoanCalcResult;
    vm.showSavingsCalculator = showSavingsCalculator;
    vm.showSavingsCalcResult = showSavingsCalcResult;
    vm.loanCalcData = {};
    vm.savingsCalcData = {};
    vm.toolModal;
    vm.resultsModal;

    activate();

    function activate() {
      analyticsService.setViewAnalytics($state.current.name);
      $scope.$on('modal.hidden', function() {
        vm.toolModal.isShown() ? vm.resultsModal.remove() : vm.toolModal.remove();
      });

      window.addEventListener('native.keyboardshow', function() {
        document.getElementsByClassName('keyboard-hide')[0].classList.add('mg-keyboard-open');
      });

      window.addEventListener('native.keyboardhide', function() {
        document.getElementsByClassName('mg-keyboard-open')[0].classList.add('mg-keyboard-closing');
        document.getElementsByClassName('mg-keyboard-open')[0].classList.remove('mg-keyboard-open');

        $timeout(function() {
          document.getElementsByClassName('mg-keyboard-closing')[0].classList.remove('mg-keyboard-closing');
          });
        }, 175);
    }

    function showLoanCalculator() {
      analyticsService.setViewAnalytics("loan-calculator");
      showModal('app/tools/loan-calculator/loan-calculator.html')
        .then(function(modal) {
           vm.toolModal = modal;
        });
    }

    function showLoanCalcResult() {
      vm.loanCalcData.payment = loanCalculatorService.calculatePayment(vm.loanCalcData);
      showModal('app/tools/loan-calculator/results.html')
        .then(function(modal) {
          vm.resultsModal = modal;
        });
    }

    function showSavingsCalculator() {
      analyticsService.setViewAnalytics("savings-calculator");
      showModal('app/tools/savings-calculator/savings-calculator.html')
        .then(function(modal) {
           vm.toolModal = modal;
        });
    }

    function showSavingsCalcResult() {
      vm.savingsCalcData.savingsPerYear = savingsCalculatorService.calculateSavings(vm.savingsCalcData);
      showModal('app/tools/savings-calculator/results.html')
        .then(function(modal) {
          vm.resultsModal = modal;
        });
    }

    function showModal(templateUrl) {
      var modal = $q.defer();

      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope
      })
      .then(function(_modal_) {
        modal.resolve(_modal_);
        _modal_.show();
      });

      return modal.promise;
    }

  }

})();

