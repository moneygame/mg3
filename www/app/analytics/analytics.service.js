(function() {
  'use strict';

  angular
    .module('moneygame.analytics')
    .service('analyticsService', analyticsService);

  /* @ngInject */
  function analyticsService() {

    this.setViewAnalytics = setViewAnalytics;
    this.setCountTries = setCountTries;

    function setViewAnalytics(view){
      if(typeof analytics !== 'undefined') {
        analytics.trackView(view);
      }
    }
    function setCountTries(category, action, label, value){
      if(typeof analytics !== 'undefined') {
        analytics.trackEvent(category, action, label, value);
      }
    }

  }

})();
