(function() {
  'use strict';

  angular
    .module('moneygame')
    .controller('ModulesMenuController', ModulesMenuController);

  /* @ngInject */
  function ModulesMenuController($ionicModal, $scope, $q, $state, modulesMenuService, analyticsService, gameService, updateManagerService) {

    var vm = this;
    vm.showModulesMenu = showModulesMenu;
    vm.goToGameModule = goToGameModule;

    activate();

    function activate() {
      updateManagerService.downloadUpdates();
      analyticsService.setViewAnalytics($state.current.name);
      modulesMenuService.getModulesList()
        .then(function(modules) {
          vm.modules = modules;
        });
    }

    function showModulesMenu() {
      showModal('app/home/modules-menu.html')
        .then(function(modal) {
           vm.modulesModal = modal;
        });
    }

    function showModal(templateUrl) {
      var modal = $q.defer();

      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope
      })
      .then(function(_modal_) {
        modal.resolve(_modal_);
        _modal_.show();
      });

      return modal.promise;
    }

    function goToGameModule(module) {
      var currentModule = {
        moduleName: module.name,
        module: module.id
      };
      vm.modulesModal.remove();
      gameService.setCurrentModule(currentModule);
      $state.go('game');
    }

  }

})();

