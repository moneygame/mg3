(function() {
  'use strict';

  angular
    .module('moneygame')
    .service('modulesMenuService', modulesMenuService);

  /* @ngInject */
  function modulesMenuService($http, HOST) {

    this.getModulesList = getModulesList;

    function getModulesList() {
      return $http.get(HOST + 'game_modules.json')
        .then(function(response) {
          return response.data.game_modules;
        });
    }

  }

})();

