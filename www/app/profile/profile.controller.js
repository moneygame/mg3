(function() {
  'use strict';

  angular
    .module('moneygame.profile')
    .controller('ProfileController', ProfileController);

  /* @ngInject */
  function ProfileController(profileService, $state, $ionicLoading, authService, analyticsService) {

    var vm = this;
    vm.loggedIn = false;
    vm.toggleLogin = toggleLogin;
    vm.user;
    vm.getModuleLevel = getModuleLevel;

    activate();

    function activate() {
      profileService.getUser()
        .then(function(user) {
          vm.user = user;
          vm.loggedIn = !!vm.user.player.facebook_id;
        });

      profileService.getModuleList()
        .then(function(modules) {
          vm.modules = modules;
        });
      analyticsService.setViewAnalytics($state.current.name);
    }

    function toggleLogin() {
      vm.loggedIn ? loginFB() : logoutFB();
    }

    function logoutFB() {
      profileService.logout(logoutFBSuccess);
    }

    function logoutFBSuccess(response) {
      $state.go('login');
      return response;
    }

    function loginFB() {
      $ionicLoading.show({
        template: 'Logging in...'
      });

      authService.loginFB(loginFBSuccess, loginFBFailure);
    }

    function loginFBSuccess(response) {
      $ionicLoading.hide();
      $state.go('home');
    }

    function loginFBFailure(response) {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Error',
        template: 'Login Failed. Try again.'
      });
    }

    function getModuleLevel(moduleId) {
      var moduleLevel = filterModuleLevel(moduleId);
      return moduleLevel.level;
    }

    function filterModuleLevel(moduleId) {
      return vm.user.player.module_levels.filter(function(moduleLevel) {
        return (moduleLevel.game_module_id == moduleId);
      })[0];
    }

  }

})();

