(function() {
  'use strict';

  angular
    .module('moneygame.profile')
    .service('profileService', profileService);

  /* @ngInject */
  function profileService(userService, modulesMenuService) {

    this.getModuleList = getModuleList;
    this.getUser = getUser;
    this.logout = logout;

    function getModuleList() {
      return modulesMenuService.getModulesList();
    }

    function getUser() {
      return userService.getUser();
    }

    function logout(successCallback, failureCallback) {
      facebookConnectPlugin.logout(
          function(response) {
            successCallback(response);
            userService.logout();
          },
          function(response) {
            if (failureCallback) {
              failureCallback(response);
            }
          });
    }

  }

})();


