# MoneyGame3

Requirements: npm, ionic, cordova, android sdk

## Installation
1. `$ git clone https://bitbucket.org/mbpernix/mg3.git`
2. `$ cd mg3`
3. `$ npm install && ionic state restore`
4. `$ ionic prepare`

### Running MoneyGame3
#### Android
`$ ionic run android`

(with android device connected and with usb debugging enabled, or android emulator running)

#### Browser
`$ ionic serve`

